Source: android-platform-system-core
Section: devel
Priority: optional
Maintainer: Android Tools Maintainers <android-tools-devel@lists.alioth.debian.org>
Uploaders: Hans-Christoph Steiner <hans@eds.org>,
           Kai-Chung Yan <seamlik@debian.org>,
           Chirayu Desai <chirayudesai1@gmail.com>,
           Umang Parmar <umangjparmar@gmail.com>
Build-Depends:
 bash-completion,
 debhelper (>= 11~),
 dh-exec,
 dpkg-dev (>= 1.17.14)
Build-Depends-Arch:
 android-libboringssl-dev [amd64 i386 armel armhf arm64],
 android-libf2fs-utils-dev (>= 8.1.0+r23~) [amd64 i386 armel armhf arm64] <!stage1>,
 android-libnativehelper-dev (>= 8.1.0+r23) [amd64 i386 armel armhf arm64 mips mipsel mips64el] <!stage1>,
 android-libunwind-dev (>= 8.1.0+r23~) [amd64 i386 armel armhf arm64 mips mipsel mips64el],
 libgtest-dev [amd64 i386 armel armhf arm64] <!stage1>,
 libsafe-iop-dev [amd64 i386 armel armhf arm64 mips mipsel mips64el],
 libusb-1.0-0-dev [amd64 i386 armel armhf arm64],
 pandoc [amd64 i386 armel armhf arm64],
 zlib1g-dev [any]
Standards-Version: 4.2.1
Homepage: https://android.googlesource.com/platform/system/core
Vcs-Git: https://salsa.debian.org/android-tools-team/android-platform-system-core.git
Vcs-Browser: https://salsa.debian.org/android-tools-team/android-platform-system-core

Package: android-sdk-libsparse-utils
Architecture: any
Multi-Arch: foreign
Section: utils
Depends: python, ${shlibs:Depends}, ${misc:Depends}
Breaks: android-tools-fsutils,
        append2simg (<< 1:8.1.0+r23-1~),
        img2simg (<< 1:8.1.0+r23-1~),
        simg2img (<< 1:8.1.0+r23-1~)
Replaces: android-tools-fsutils,
          append2simg (<< 1:8.1.0+r23-1~),
          img2simg (<< 1:8.1.0+r23-1~),
          simg2img (<< 1:8.1.0+r23-1~)
Description: Android sparse image creation tool
 Command line tools to create sparse images for usage with Android devices.
 Includes simgimg, img2simg, simg2simg, simg_dump and append2simg tools.

Package: android-liblog
Section: libs
Architecture: any
Depends: ${misc:Depends}, ${shlibs:Depends}
Description: Android NDK logger interfaces
 liblog represents an interface to the volatile Android Logging system for NDK
 (Native) applications and libraries. Interfaces for either writing or reading
 logs. The log buffers are divided up in Main, System, Radio and Events
 sub-logs.

Package: android-liblog-dev
Section: libdevel
Architecture: any
Depends: android-liblog (= ${binary:Version}),
         ${misc:Depends}
Breaks: android-liblog (<< 21-4~)
Replaces: android-liblog (<< 21-4~)
Description: Android NDK logger interfaces - Development files
 liblog represents an interface to the volatile Android Logging system for NDK
 (Native) applications and libraries. Interfaces for either writing or reading
 logs. The log buffers are divided up in Main, System, Radio and Events
 sub-logs.
 .
 This package contains the development files.

Package: android-libcutils
Section: libs
Architecture: any
Depends: android-liblog (= ${binary:Version}),
         libbsd0,
         ${shlibs:Depends},
         ${misc:Depends}
Description: Android utils library for C
 This library provides common functionalities for android related tools.
 .
 This library is only used by Android SDK currently.

Package: android-libcutils-dev
Section: libdevel
Architecture: any
Depends: ${misc:Depends},
         android-libcutils (= ${binary:Version}),
         android-liblog-dev (= ${binary:Version}),
         android-platform-system-core-headers (= ${source:Version}),
         libbsd-dev
Breaks: android-libcutils (<< 21-4~),
        android-liblog-dev (<< 1:6)
Replaces: android-libcutils (<< 21-4~),
          android-liblog-dev (<< 1:6)
Description: Android utils library for C - Development files
 This library provides common functionalities for android related tools.
 .
 This library is only used by Android SDK currently.
 .
 This package contains the development files.

Package: adb
Architecture: amd64 i386 armel armhf arm64
Multi-Arch: foreign
Depends: android-libadb (= ${binary:Version}),
         ${shlibs:Depends},
         ${misc:Depends}
Recommends: android-sdk-platform-tools-common
Breaks: android-tools-adb (<< 6.0~)
Replaces: android-tools-adb (<< 6.0~)
Provides: android-tools-adb
Description: Android Debug Bridge
 A versatile command line tool that lets you communicate with an emulator
 instance or connected Android-powered device.
 .
 This package recommends "android-sdk-platform-tools-common" which contains
 the udev rules for Android devices. Without this package, adb and fastboot need
 to be running with root permission.

Package: android-libsparse
Section: libs
Architecture: any
Depends: ${misc:Depends}, ${shlibs:Depends}
Description: Library for sparse files
 This library provides APIs for creating, manipulating and destroying sparse
 files.
 .
 This library is only used by Android SDK currently.

Package: android-libsparse-dev
Section: libdevel
Architecture: any
Depends: android-libsparse (= ${binary:Version}), ${misc:Depends}
Description: Library for sparse files - Development files
 This library provides APIs for creating, manipulating and destroying sparse
 files.
 .
 This library is only used by Android SDK currently.
 .
 This package contains the development files.

Package: android-libutils
Section: libs
Architecture: amd64 i386 armel armhf arm64 mips mipsel mips64el
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: Android Utility Function Library
 This library provides miscellaneous utility functions.
 .
 This library is only used by Android SDK currently.

Package: android-libutils-dev
Section: libdevel
Architecture: amd64 i386 armel armhf arm64 mips mipsel mips64el
Depends: android-libbacktrace-dev (>= ${binary:Version}),
         android-libcutils-dev (>= ${binary:Version}),
         android-libutils (= ${binary:Version}),
         android-platform-system-core-headers (>= ${source:Version}),
         ${misc:Depends}
Description: Android Utility Function Library - Development files
 This library provides miscellaneous utility functions.
 .
 This library is only used by Android SDK currently.
 .
 This package contains the development files.

Package: android-libziparchive
Section: libs
Architecture: amd64 i386 armel armhf arm64 mips mipsel mips64el
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: Library for ZIP archives
 This library provides APIs for creating and manipulating ZIP archives.
 .
 This library is only used by Android SDK currently.

Package: android-libziparchive-dev
Section: libdevel
Architecture: amd64 i386 armel armhf arm64 mips mipsel mips64el
Depends: android-libziparchive (= ${binary:Version}), ${misc:Depends}
Description: Library for ZIP archives - Development files
 This library provides APIs for creating and manipulating ZIP archives.
 .
 This library is only used by Android SDK currently.
 .
 This package contains the development files.

Package: android-platform-system-core-headers
Section: libdevel
Architecture: all
Depends: ${misc:Depends}
Breaks: android-system-dev
Replaces: android-system-dev
Provides: android-system-dev
Description: Shared headers in AOSP repository platform/system/core
 This package contains header files in AOSP repository platform/system/core that
 do not belong to any specific libraries but used by other programs.
 .
 This package currently contains headers in include/private and include/system.

Package: android-libbacktrace
Section: libs
Architecture: amd64 i386 armel armhf arm64 mips mipsel mips64el
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: Android backtrace library
 This library is only used by Android SDK currently.

Package: android-libbacktrace-dev
Section: libdevel
Architecture: amd64 i386 armel armhf arm64 mips mipsel mips64el
Depends: android-libbacktrace (= ${binary:Version}), ${misc:Depends}
Description: Android backtrace library - Development files
 This library is only used by Android SDK currently.
 .
 This package contains the development files.

Package: android-libadb
Section: libs
Architecture: amd64 i386 armel armhf arm64
Depends: android-libbase (= ${binary:Version}),
         ${shlibs:Depends},
         ${misc:Depends}
Description: Library for Android Debug Bridge
 This library provides APIs for accessing and controlling Android devices.
 .
 This library is only used by Android SDK currently.

Package: android-libadb-dev
Section: libdevel
Architecture: amd64 i386 armel armhf arm64
Depends: android-libadb (= ${binary:Version}),
         android-liblog-dev (= ${binary:Version}),
         ${misc:Depends}
Description: Library for Android Debug Bridge - Development files
 This library provides APIs for accessing and controlling Android devices.
 .
 This library is only used by Android SDK currently.
 .
 This package contains the development files.

Package: android-libbase
Section: libs
Architecture: any
Depends: android-liblog (= ${binary:Version}),
         ${shlibs:Depends},
         ${misc:Depends}
Description: Android base library
 This library provides APIs for basic tasks like handling files, Unicode
 strings, logging, memory allocation, integer parsing, etc..
 .
 This library is only used by Android SDK currently.

Package: android-libbase-dev
Section: libdevel
Architecture: any
Depends: android-libbase (= ${binary:Version}), ${misc:Depends}
Description: Android base library - Development files
 This library provides APIs for basic tasks like handling files, Unicode
 strings, logging, memory allocation, integer parsing, etc..
 .
 This library is only used by Android SDK currently.
 .
 This package contains the development files.

Package: android-libcrypto-utils
Section: libs
Architecture: amd64 i386 armel armhf arm64
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: Android crypto-utils library
 This library provides utility APIs for cryptography.
 .
 This library is only used by Android SDK currently.

Package: android-libcrypto-utils-dev
Section: libdevel
Architecture: amd64 i386 armel armhf arm64
Depends: android-libcrypto-utils (= ${binary:Version}), ${misc:Depends}
Description: Android crypto-utils library - Development files
 This library provides utility APIs for cryptography.
 .
 This library is only used by Android SDK currently.
 .
 This package provides the development files.

Package: simg2img
Depends: android-sdk-libsparse-utils, ${misc:Depends}
Architecture: all
Section: oldlibs
Description: Transitional package
 This is a transitional package. It can safely be removed.

Package: img2simg
Architecture: all
Section: oldlibs
Depends: android-sdk-libsparse-utils, ${misc:Depends}
Description: Transitional package
 This is a transitional package. It can safely be removed.

Package: append2simg
Depends: android-sdk-libsparse-utils, ${misc:Depends}
Architecture: all
Section: oldlibs
Description: Transitional package
 This is a transitional package. It can safely be removed.

Package: android-tools-adb
Depends: adb, ${misc:Depends}
Architecture: all
Section: oldlibs
Description: transitional package
 This is a transitional package. It can safely be removed.

Package: android-tools-fastboot
Depends: fastboot, ${misc:Depends}
Architecture: all
Section: oldlibs
Description: transitional package
 This is a transitional package. It can safely be removed.

Package: android-tools-mkbootimg
Depends: mkbootimg, ${misc:Depends}
Architecture: all
Section: oldlibs
Description: transitional package
 This is a transitional package. It can safely be removed.

Package: mkbootimg
Depends: python3, ${misc:Depends}
Breaks: android-tools-mkbootimg (<< 1:8.1.0+r23-1~)
Replaces: android-tools-mkbootimg (<< 1:8.1.0+r23-1~)
Architecture: all
Section: utils
Description: Creates Android boot images
 Creates Android boot images that includes kernel image and ramdisk, in a
 special format which can be used with fastboot.

Package: fastboot
Architecture: amd64 i386 armel armhf arm64
Multi-Arch: foreign
Build-Profiles: <!stage1>
Depends: ${shlibs:Depends}, ${misc:Depends}
Recommends: android-sdk-platform-tools
Breaks: android-tools-fastboot (<< 6.0~)
Replaces: android-tools-fastboot (<< 6.0~)
Provides: android-tools-fastboot
Description: Android fastboot tool
 A command line tool for flashing an Android device, boot an Android device to
 fastboot mode, etc..

Package: android-libnativebridge
Section: libs
Architecture: amd64 i386 armel armhf arm64 mips mipsel mips64el
Build-Profiles: <!stage1>
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: Android native bridge library
 This library is only used by Android SDK currently.

Package: android-libnativebridge-dev
Section: libdevel
Architecture: amd64 i386 armel armhf arm64 mips mipsel mips64el
Build-Profiles: <!stage1>
Depends: android-libnativebridge (= ${binary:Version}),
         android-libnativehelper-dev,
         ${misc:Depends}
Description: Android native bridge library - Development files
 This library is only used by Android SDK currently.
 .
 This package provides the development files.

Package: android-libnativeloader
Section: libs
Architecture: amd64 i386 armel armhf arm64 mips mipsel mips64el
Build-Profiles: <!stage1>
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: Android native loader library
 This library is only used by Android SDK currently.

Package: android-libnativeloader-dev
Section: libdevel
Architecture: amd64 i386 armel armhf arm64 mips mipsel mips64el
Build-Profiles: <!stage1>
Depends: android-libnativehelper-dev,
         android-libnativeloader (= ${binary:Version}),
         ${misc:Depends}
Description: Android native loader library - Development files
 This library is only used by Android SDK currently.
 .
 This package provides the development files.